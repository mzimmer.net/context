package net.mzimmer.context.completeablefuture;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import net.mzimmer.context.Container;
import net.mzimmer.context.Context;

import java.util.concurrent.CompletableFuture;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CompletableFutureContext implements Context<CompletableFutureContext> {
    private static final CompletableFutureContext INSTANCE = new CompletableFutureContext();

    public static CompletableFutureContext completableFutureContext() {
        return INSTANCE;
    }

    @Override
    public <T> CompletableFutureContainer<T> pure(T value) {
        return new CompletableFutureContainer<>(CompletableFuture.completedFuture(value));
    }

    public <T> CompletableFutureContainer<T> of(CompletableFuture<T> completableFuture) {
        return new CompletableFutureContainer<>(completableFuture);
    }

    @SneakyThrows
    @Override
    public <T> T get(Container<CompletableFutureContext, T> container) {
        return underlying(container).get();
    }

    public <T> CompletableFuture<T> underlying(Container<CompletableFutureContext, T> container) {
        return implementation(container).getValue();
    }

    private <T> CompletableFutureContainer<T> implementation(Container<CompletableFutureContext, T> container) {
        return (CompletableFutureContainer<T>) container;
    }
}
