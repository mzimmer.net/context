package net.mzimmer.context.completeablefuture;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.mzimmer.context.Container;
import net.mzimmer.context.Context;

import java.util.concurrent.CompletableFuture;
import java.util.function.Function;

import static net.mzimmer.context.completeablefuture.CompletableFutureContext.completableFutureContext;

@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class CompletableFutureContainer<T> implements Container<CompletableFutureContext, T> {
    @Getter(value = AccessLevel.PACKAGE)
    private final CompletableFuture<T> value;

    @Override
    public Context<CompletableFutureContext> context() {
        return completableFutureContext();
    }

    @Override
    public <U> Container<CompletableFutureContext, U> map(Function<T, U> f) {
        return completableFutureContext().of(value.thenApplyAsync(f));
    }

    @Override
    public <U> Container<CompletableFutureContext, U> flatMap(Function<T, Container<CompletableFutureContext, U>> f) {
        return completableFutureContext().of(value.thenComposeAsync(f.andThen(completableFutureContext()::underlying)));
    }
}
