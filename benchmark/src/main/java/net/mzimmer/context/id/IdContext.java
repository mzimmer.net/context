package net.mzimmer.context.id;

import net.mzimmer.context.Container;
import net.mzimmer.context.Context;

public class IdContext implements Context<IdContext> {
    private static final IdContext INSTANCE = new IdContext();

    private IdContext() {
    }

    public static IdContext idContext() {
        return INSTANCE;
    }

    @Override
    public <T> IdContainer<T> pure(T value) {
        return new IdContainer<>(value);
    }

    public <T> IdContainer<T> of(T value) {
        return new IdContainer<>(value);
    }

    @Override
    public <T> T get(Container<IdContext, T> container) {
        return underlying(container);
    }

    public <T> T underlying(Container<IdContext, T> container) {
        return implementation(container).getValue();
    }

    private <T> IdContainer<T> implementation(Container<IdContext, T> container) {
        return (IdContainer<T>) container;
    }
}
