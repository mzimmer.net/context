package net.mzimmer.context.id;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.mzimmer.context.Container;
import net.mzimmer.context.Context;

import java.util.function.Function;

import static net.mzimmer.context.id.IdContext.idContext;

@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class IdContainer<T> implements Container<IdContext, T> {
    @Getter(value = AccessLevel.PACKAGE)
    private final T value;

    @Override
    public Context<IdContext> context() {
        return idContext();
    }

    @Override
    public <U> Container<IdContext, U> flatMap(Function<T, Container<IdContext, U>> f) {
        return f.apply(value);
    }
}
