package net.mzimmer.context.benchmark;

import lombok.AllArgsConstructor;
import net.mzimmer.context.Container;
import net.mzimmer.context.Context;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import static net.mzimmer.context.completeablefuture.CompletableFutureContext.completableFutureContext;
import static net.mzimmer.context.id.IdContext.idContext;
import static net.mzimmer.context.rxjava3.SingleContext.singleContext;

public class Benchmarks {
    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
            .include(Benchmarks.class.getSimpleName())
            .forks(1)
            .build();

        new Runner(opt).run();
    }

    @Benchmark
    public void benchmarkIdContext() {
        benchmarkContext(idContext());
    }

    @Benchmark
    public void benchmarkSingleContext() {
        benchmarkContext(singleContext());
    }

    @Benchmark
    public void benchmarkCompletableFutureContext() {
        benchmarkContext(completableFutureContext());
    }

    <C extends Context<C>> void benchmarkContext(C context) {
        // given
        final Passive<C> passive = new Passive<>(context);
        final Domain<C> domain = new Domain<>(context);
        final Application<C> application = new Application<>(domain, passive);
        final Active<C> active = new Active<>(application);

        // when
        final String result = active.get();

        // then
        assert result.startsWith("Result: Data: ");
    }

    @AllArgsConstructor
    private static class Passive<C extends Context<C>> {
        private final Context<C> context;

        public Container<C, Integer> get() {
            return context.pure(9001);
        }
    }

    @AllArgsConstructor
    private static class Domain<C extends Context<C>> {
        private final Context<C> context;

        public Container<C, String> calculate(int data) {
            return context.pure("Data: " + data);
        }
    }

    @AllArgsConstructor
    private static class Application<C extends Context<C>> {
        private final Domain<C> domain;
        private final Passive<C> passive;

        public Container<C, String> get() {
            final Container<C, Integer> data = passive.get();
            return data.flatMap(domain::calculate);
        }
    }

    @AllArgsConstructor
    private static class Active<C extends Context<C>> {
        private final Application<C> application;

        public String get() {
            final Container<C, String> eventualCommandResult = application.get();
            final Container<C, String> eventualOutputModel = eventualCommandResult.map(r -> "Result: " + r);
            return eventualOutputModel.get();
        }
    }
}
