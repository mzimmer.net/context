package net.mzimmer.context.rxjava3;

import io.reactivex.rxjava3.core.Single;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import net.mzimmer.context.Container;
import net.mzimmer.context.Context;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class SingleContext implements Context<SingleContext> {
    private static final SingleContext INSTANCE = new SingleContext();

    public static SingleContext singleContext() {
        return INSTANCE;
    }

    @Override
    public <T> SingleContainer<T> pure(T value) {
        return new SingleContainer<>(Single.just(value));
    }

    public <T> SingleContainer<T> of(Single<T> single) {
        return new SingleContainer<>(single);
    }

    @Override
    public <T> T get(Container<SingleContext, T> container) {
        return underlying(container).blockingGet();
    }

    public <T> Single<T> underlying(Container<SingleContext, T> container) {
        return implementation(container).getValue();
    }

    private <T> SingleContainer<T> implementation(Container<SingleContext, T> container) {
        return (SingleContainer<T>) container;
    }
}
