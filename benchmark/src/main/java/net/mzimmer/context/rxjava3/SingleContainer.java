package net.mzimmer.context.rxjava3;

import io.reactivex.rxjava3.core.Single;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.mzimmer.context.Container;
import net.mzimmer.context.Context;

import java.util.function.Function;

import static net.mzimmer.context.rxjava3.SingleContext.singleContext;

@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class SingleContainer<T> implements Container<SingleContext, T> {
    @Getter(value = AccessLevel.PACKAGE)
    private final Single<T> value;

    @Override
    public Context<SingleContext> context() {
        return singleContext();
    }

    @Override
    public <U> Container<SingleContext, U> map(Function<T, U> f) {
        return singleContext().of(value.map(f::apply));
    }

    @Override
    public <U> Container<SingleContext, U> flatMap(Function<T, Container<SingleContext, U>> f) {
        return singleContext().of(value.flatMap(f.andThen(singleContext()::underlying)::apply));
    }
}
