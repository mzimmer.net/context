package net.mzimmer.context;

public interface Context<C extends Context<C>> {
    <T> Container<C, T> pure(T value);

    <T> T get(Container<C, T> container);
}
