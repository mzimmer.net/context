package net.mzimmer.context;

import java.util.function.Function;

public interface Container<C extends Context<C>, T> {
    Context<C> context();

    default <U> Container<C, U> map(Function<T, U> f) {
        return flatMap(f.andThen(context()::pure));
    }

    <U> Container<C, U> flatMap(Function<T, Container<C, U>> f);

    default T get() {
        return context().get(this);
    }
}
